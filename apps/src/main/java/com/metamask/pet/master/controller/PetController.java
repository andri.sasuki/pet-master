package com.metamask.pet.master.controller;

import com.metamask.pet.master.service.PetService;
import com.metamask.pet.master.persistence.domain.Pet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/v1/pet")
public class PetController {

    @Autowired
    private PetService petService;

    @GetMapping("/all")
    public List<Pet> findAll() {
        return petService.findAll();
    }

    @GetMapping("/find/{id}")
    public Pet findById(@PathVariable("id") Long id) {
        return petService.findById(id);
    }

    @GetMapping("/map/{id}")
    public Map<String, Object> mapById(@PathVariable("id") Long id) {
        return petService.findMapById(id);
    }

    @GetMapping("/map")
    public List<Map<String, Object>> mapAll() {
        return petService.findMapAll();
    }

    @PostMapping("/add")
    public Pet add(@RequestBody Pet pet) {
        return petService.add(pet);
    }

}
