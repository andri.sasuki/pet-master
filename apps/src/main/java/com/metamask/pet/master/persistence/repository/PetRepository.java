package com.metamask.pet.master.persistence.repository;

import com.metamask.pet.master.persistence.domain.Pet;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PetRepository extends CrudRepository<Pet, Long> {
}
