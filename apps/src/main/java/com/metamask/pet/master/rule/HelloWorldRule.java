package com.metamask.pet.master.rule;

import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Rule;

@Rule(name = "Hello World rule", description = "Always say hello world")
public class HelloWorldRule {

    private String result;

    @Condition
    public boolean when() {
        return true;
    }

    @Action
    public void then() throws Exception {
        System.out.println("my rule has been executed");
        result = "my rule has been executed"; // assign your result here

    }

    public String getResult() {
        return this.result;
    }

}
