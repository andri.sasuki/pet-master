package com.metamask.pet.master.service;

import com.metamask.greeter.Greeter;
import com.metamask.pet.master.persistence.domain.Pet;
import com.metamask.pet.master.persistence.domain.Pet_;
import com.metamask.pet.master.persistence.mapper.PetMapRowMapper;
import com.metamask.pet.master.persistence.mapper.PetRowMapper;
import com.metamask.pet.master.persistence.repository.PetRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class PetService {

    @Autowired
    private PetRepository petRepository;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    private Greeter greeter;

    public Pet add(Pet pet) {
        log.info("add new pet {}: {}", Pet_.NAME, pet.getName());
        return petRepository.save(pet);
    }

    public List<Pet> findAll() {
        log.info("find all pet");
        return StreamSupport
                .stream(petRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    public Pet findById(Long id) {
        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("id", id);
        String sql = "SELECT * FROM pets WHERE id = :id";
        try {
            Pet pet = namedParameterJdbcTemplate.queryForObject(sql, in, new PetRowMapper());
            return pet;
        } catch (EmptyResultDataAccessException e) {
            log.error("pet id {} not found", id);
            return null;
        }
    }

    public List<Map<String, Object>> findMapAll() {
        String sql = "SELECT id, age, name, owner_id, is_active FROM pets";
        List<Map<String, Object>> pets = namedParameterJdbcTemplate.query(sql,
                new MapSqlParameterSource(), new PetMapRowMapper());
        return pets;

    }

    public Map<String, Object> findMapById(Long id) {
        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("id", id);
        String sql = "SELECT * FROM pets WHERE id = :id";
        try {
            Map<String, Object> pet = namedParameterJdbcTemplate.queryForObject(sql, in, new PetMapRowMapper());
            return pet;
        } catch (EmptyResultDataAccessException e) {
            log.error("map pet id {} not found", id);
            return null;
        }
    }

}
