package com.metamask.pet.master.persistence.mapper;

import com.metamask.pet.master.persistence.domain.Pet;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PetRowMapper implements RowMapper<Pet> {

    @Override
    public Pet mapRow(ResultSet rs, int rowNum) throws SQLException {
        Pet pet = new Pet();
        pet.setId(rs.getLong("id"));
        pet.setName(rs.getString("name"));
        pet.setAge(rs.getInt("age"));
        pet.setActive(rs.getBoolean("is_active"));
        pet.setCreatedAt(rs.getTimestamp("created_at"));
        return pet;
    }
}
