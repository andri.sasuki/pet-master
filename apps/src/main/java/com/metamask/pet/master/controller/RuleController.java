package com.metamask.pet.master.controller;

import com.metamask.pet.master.service.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/rule")
public class RuleController {
    
    @Autowired
    RuleService ruleService;

    @GetMapping("/trigger")
    public String trigget() {
        return ruleService.trigger();
    }

}
