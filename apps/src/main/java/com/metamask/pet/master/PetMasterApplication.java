package com.metamask.pet.master;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetMasterApplication {

	public static void main(String[] args) {
		SpringApplication.run(PetMasterApplication.class, args);
	}

}
