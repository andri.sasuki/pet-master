package com.metamask.pet.master.controller;

import com.metamask.pet.master.service.PetService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.util.pattern.PathPatternParser;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Create dynamic controller mappings
 * Ref :
 * https://www.javacodegeeks.com/2022/04/fixing-the-spring-web-error-expected-lookuppath-in-request-attribute-org-springframework-web-util-urlpathhelper-path.html
 * https://stackoverflow.com/questions/5758504/is-it-possible-to-dynamically-set-requestmappings-in-spring-mvc
 */
@Slf4j
@SuppressWarnings("unchecked")
@Controller
public class DynamicApiController {

    @Autowired
    private RequestMappingHandlerMapping requestMappingHandlerMapping;

    @Autowired
    private PetService petService;

    @PostConstruct
    public void init() throws NoSuchMethodException {
        RequestMappingInfo.BuilderConfiguration options = new RequestMappingInfo.BuilderConfiguration();
        options.setPatternParser(new PathPatternParser());

        requestMappingHandlerMapping.registerMapping(
                RequestMappingInfo.paths("/pet/info/{id}")
                        .mappingName("pet-id")
                        .methods(RequestMethod.GET)
                        .produces(MediaType.APPLICATION_JSON_VALUE)
                        .options(options)
                        .build(),
                this,
                DynamicApiController.class.getDeclaredMethod("findPetById", String.class,
                        HttpServletRequest.class));

        requestMappingHandlerMapping.registerMapping(
                RequestMappingInfo.paths("/simple-handler")
                        .methods(RequestMethod.GET)
                        .produces(MediaType.APPLICATION_JSON_VALUE)
                        .options(options)
                        .build(),
                this,
                DynamicApiController.class.getDeclaredMethod("simpleHandler",
                        HttpServletRequest.class));
    }

    // [GET] /pet/info/{id}
    public ResponseEntity<Map<String, Object>> findPetById(
            @RequestParam(value = "name", required = true) String name,
            HttpServletRequest httpServletRequest) {
        Map<String, Object> mp = (Map<String, Object>) httpServletRequest
                .getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);

        String apiName = httpServletRequest
                        .getAttribute("org.springframework.web.servlet.HandlerMapping.bestMatchingPattern").toString();
        log.info("attribute name {}", apiName);

        // Get query param from HttpServletRequest
        // String name = httpServletRequest.getParameter("name");
        log.info("find pet id {} name {}", mp.get("id"), name);
        if (!mp.containsKey("id") || name == null || name.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new HashMap<>());
        }
        Long petId = Long.valueOf(mp.get("id").toString());
        return ResponseEntity.status(HttpStatus.OK).body(petService.findMapById(petId));
    }

    // [GET] /simpleHandler
    public ResponseEntity<List<String>> simpleHandler(HttpServletRequest httpServletRequest) {
        Map<String, Object> attrs = (Map<String, Object>) httpServletRequest
                .getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        log.info("path variables {}", attrs);
        return ResponseEntity.status(HttpStatus.OK).body(Arrays.asList("simple"));
    }

}
