package com.metamask.greeter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class GreeterController {

    @Autowired
    private Greeter greeter;

    @GetMapping("/greeter")
    public ResponseEntity<Map<String, String>> echo() {
        Map<String, String> res = Map.of("greet", greeter.greet());
        return ResponseEntity.ok(res);
    }

}
