package com.metamask.pet.master.service;

import com.metamask.pet.master.rule.HelloWorldRule;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.springframework.stereotype.Service;

@Service
public class RuleService {

    public String trigger() {
        Facts facts = new Facts();
        // create rules
        Rules rules = new Rules();
        HelloWorldRule helloWorldRule = new HelloWorldRule();
        rules.register(helloWorldRule);
        // create a rules engine and fire rules on known facts
        RulesEngine rulesEngine = new DefaultRulesEngine();
        rulesEngine.fire(rules, facts);
        
        return helloWorldRule.getResult();
    }

}
