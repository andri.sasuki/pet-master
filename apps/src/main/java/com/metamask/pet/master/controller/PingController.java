package com.metamask.pet.master.controller;

import com.metamask.greeter.Greeter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class PingController {

    @Autowired
    Greeter greeter;

    @GetMapping("/ping")
    public ResponseEntity<Map<String, String>> ping() {
        Map<String, String> pong = Map.of("pong", greeter.greet());
        return ResponseEntity.ok(pong);
    }


}
