package com.metamask.pet.master.persistence.repository;

import com.metamask.pet.master.persistence.domain.Owner;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OwnerRepository extends CrudRepository<Owner, Long> {
}
